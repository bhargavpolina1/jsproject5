function cacheFunction(cb) {
    

let cacheObject = {}
function samplecache(...args){
    if (args in cacheObject){
        return cacheObject[args]
    }
    else{
        let result = cb(...args)
        cacheObject[args] = result
        return result
}

}

return samplecache
}


module.exports = cacheFunction;


