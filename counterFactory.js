function counterFactory(){

    let counter = 1
    let counterObject = { 
        increment: () => counter = counter+1,
        decrement: () => counter = counter-1
    }
    return(counterObject)
   
}  




module.exports = counterFactory;