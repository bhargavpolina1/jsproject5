let limitFunctionCallCount = require('../limitFunctionCallCount.js')




const add = (a,b) => a + b;

const limitedAddFn = limitFunctionCallCount(add, 2);

console.log(limitedAddFn(1,2)); // 3
console.log(limitedAddFn(3,4)); // 7
console.log(limitedAddFn(4,5)); // null


