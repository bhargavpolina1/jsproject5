function limitFunctionCallCount(cb, n) {

let attempt = 0 
function sampleCallCount(...args) {
        let result = cb(...args)
        attempt += 1

        if (attempt <= n){
            return result
        }else{
            return null
        }


    }

    return sampleCallCount
}

module.exports = limitFunctionCallCount;


